Project Amber
===================================

This is a group project for the ["Creativity, Activity, Service"][CAS] portion 
of the International Baccalaureate program.

>>>
We plan to raise awareness for neglected, abused and impoverished children by 
creating a media adaptation of Hans Christian Andersen’s “The Little Match Girl” 
in the form of a video game. Ideally, this project will make use of and further 
our skills concerning art, programming and publicity, while positively impacting 
a social cause we feel passionately for.

Our current aim is to produce a fully original, thought-provoking 5–10 minutes 
of gameplay that will be distributed as widely as possible, gauged through the 
game’s download count, which we would like to surpass 100. We hope that 
spreading our game will inspire support from our audience towards local 
organizations to combat these tragic circumstances.
>>>

Contributing
------------
See [CONTRIBUTING.md](CONTRIBUTING.md).

License
-------
This project, including scripts, models, and artwork, is licensed under the 
Mozilla Public License 2.0 or greater. If you have questions about the 
licensing, contact any of our team members or visit Mozilla's [FAQ][MPLFAQ].


[CAS]: https://www.ibo.org/programmes/diploma-programme/curriculum/creativity-activity-and-service/
[MPLFAQ]: https://www.mozilla.org/en-US/MPL/2.0/FAQ/

